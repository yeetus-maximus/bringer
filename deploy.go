package main

import (
	"fmt"
	"io/ioutil"
	"time"
)

var (
	Sum = sumFiles()
)

func WatchFiles(interval time.Duration) {
	ticker := time.NewTicker(interval * time.Minute)
	go func() {
		for _ = range ticker.C {
			if sumFiles() != Sum {
				Sum = sumFiles()
				PopulateRepositories()
			}
		}
	}()
}

func sumFiles() int {
	fileSum := 0
	if files, err := ioutil.ReadDir("./repositories/"); err == nil {
		for _, file := range files {
			if file.IsDir() {
				if versions, err := ioutil.ReadDir(fmt.Sprintf("./repositories/%s/versions/", file.Name())); err == nil {
					for range versions {
						fileSum++
					}
				}
			}
		}
	}
	return fileSum
}
