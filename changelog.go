package main

import (
	"encoding/json"
	"fmt"
	"gopkg.in/djherbis/times.v1"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
)

var (
	Changelogs  = make(map[string]*ChangelogEntry)
	VersionData = make(map[string]*VersionEntry)
)

type ChangelogEntry struct {
	Changelog []string `json:"changelog"`
	Date string `json:"date"`
}

type VersionEntry struct {
	Version   string   `json:"version"`
	Changelog []string `json:"changelog"`
	Link      string   `json:"link"`
}

func getFileDate(path string) string {
	at, err := times.Stat(path)
	if err != nil {
		log.Fatal(err.Error())
	}
	return at.ChangeTime().Format("Jan _2 2006 15:04 MST")
}

func UpdateChangelogs() {
	Changelogs = make(map[string]*ChangelogEntry)
	path := fmt.Sprintf("%s/versions/", Repositories["aristois"].Path)
	if files, err := ioutil.ReadDir(path); err == nil {
		for _, file := range files {
			if changelog, err := os.Open(fmt.Sprintf("%s%s/changelog.txt", path, file.Name())); err == nil {
				if bits, err := ioutil.ReadAll(changelog); err == nil {
					Changelogs[file.Name()] = &ChangelogEntry{
						Changelog: strings.Split(string(bits), "\n"),
						Date: getFileDate(fmt.Sprintf("%s%s/base.jar", path, file.Name())),
					}
				} else {
					log.Fatal(err)
				}
			}
		}
		var changelogJson []byte
		if changelogJson, err = json.Marshal(&Changelogs); err != nil {
			log.Fatal(err)
		}
		repositoryFile := &RepositoryFile{
			JarFile:   false,
			MavenPath: "changelog.json",
			ServeHTTP: func(w http.ResponseWriter, r *http.Request) {
				w.Header().Set("Content-Type", "application/json")
				if _, err := w.Write(changelogJson); err != nil {
					log.Fatal(err)
				}
			},
		}
		Repositories["aristois"].Versions["changelog"] = repositoryFile
	} else {
		log.Fatal(err)
	}
}

func UpdateLatestVersion() {
	VersionData = make(map[string]*VersionEntry)
	for _, suffix := range Repositories["aristois"].Suffixes {
		VersionData[suffix] = &VersionEntry{
			Version:   Repositories["aristois"].LatestVersion,
			Changelog: Changelogs[Repositories["aristois"].LatestVersion].Changelog,
			Link:      "https://aristois.net/download",
		}
	}
	var err error
	var updateJson []byte
	if updateJson, err = json.Marshal(&VersionData); err != nil {
		log.Fatal(err)
	}
	repositoryFile := &RepositoryFile{
		JarFile:   false,
		MavenPath: "update.json",
		ServeHTTP: func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "application/json")
			if _, err := w.Write(updateJson); err != nil {
				log.Fatal(err)
			}
		},
	}
	Repositories["aristois"].Versions["update"] = repositoryFile
}
