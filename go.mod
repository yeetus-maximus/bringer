module bringer

go 1.13

require (
	github.com/djherbis/atime v1.0.0
	gopkg.in/djherbis/times.v1 v1.2.0
)
