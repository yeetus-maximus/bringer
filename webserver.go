package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

var (
	Server = WebServer{
		Address: "localhost:8087",
	}
)

type WebServer struct {
	Address string
}

func ReloadVersions() {
	if jsonData, err := ioutil.ReadFile("./repositories/versions.json"); err == nil {
		repositoryFile := &RepositoryFile{
			JarFile:   false,
			MavenPath: "versions.json",
			ServeHTTP: func(w http.ResponseWriter, r *http.Request) {
				w.Header().Set("Content-Type", "application/json")
				if _, err := w.Write(jsonData); err != nil {
					log.Fatal(err)
				}
			},
		}
		Repositories["aristois"].Versions["versions"] = repositoryFile
	} else {
		log.Println(err)
	}
}

func (h *WebServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	if RepoDataLoaded {
		if r.URL.Path == "/" {
			if _, err := w.Write([]byte(IndexCache)); err != nil {
				log.Fatal(err)
			}
		} else {
			if repositoryFile, err := GetRepositoryFile(r.URL.Path[1:]); err == nil {
				repositoryFile.ServeHTTP(w, r)
			} else {
				w.WriteHeader(http.StatusNotFound)
				if _, err := w.Write([]byte("Could not find file")); err != nil {
					log.Fatal(err)
				}
			}
		}
	} else {
		if _, err := w.Write([]byte("Rebuilding cache, please reload in a few seconds")); err != nil {
			log.Fatal(err)
		}
	}
}

func ServeMavenFiles() {
	http.Handle("/", &Server)
	server := &http.Server{
		Addr: Server.Address,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
	}
	server.SetKeepAlivesEnabled(false)
	log.Println("Web server started")
	log.Fatal(server.ListenAndServe())
}
