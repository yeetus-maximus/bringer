package main

import (
	"crypto/sha1"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
)

var (
	Repositories = make(map[string]*Repository)
	RepoDataLoaded = false
	IndexCache   = ""
)

type Repository struct {
	Package            string   `json:"package"`
	PackageSuffix      string   `json:"packageSuffix"`
	Suffixes           []string `json:"suffixes"`
	SetupLatestVersion bool     `json:"latestVersion"`
	AllowIndex         bool     `json:"allowIndex"`
	ExcludeFor		   []string  `json:"excludeFor"`
	Name               string
	Versions           map[string]*RepositoryFile
	Path               string
	LatestVersion      string
}

type RepositoryFile struct {
	FilePath  string
	JarFile   bool
	MavenPath string
	Checksum  string
	Version   string
	Pom       string
	ServeHTTP func(w http.ResponseWriter, r *http.Request)
}

func (repo *Repository) buildStructure() {
	repo.Versions = make(map[string]*RepositoryFile)
	if files, err := ioutil.ReadDir(repo.Path + "/versions/"); err == nil {
		fileVersions := make([]string, 0)
		latestVersion := 0
		for _, version := range files {
			fileVersions = append(fileVersions, version.Name())
			if !stringInSlice(version.Name(), repo.ExcludeFor) {
				if v, err := strconv.Atoi(version.Name()); err == nil && repo.SetupLatestVersion {
					if latestVersion < v {
						latestVersion = v
					}
				}
			}
		}
		if repo.SetupLatestVersion {
			repo.LatestVersion = strconv.Itoa(latestVersion)
			fileVersions = append(fileVersions, "latest")
		}
		for _, version := range fileVersions {
			if len(repo.Suffixes) != 0 {
				for _, suffix := range repo.Suffixes {
					if stringInSlice(version, repo.ExcludeFor) {
						repo.appendVersion(version, version, "")
					} else {
						pathVersion := version
						if version == "latest" {
							pathVersion = strconv.Itoa(latestVersion)
						}
						repo.appendVersion(version, pathVersion, "-"+suffix)
					}
				}
			} else {
				repo.appendVersion(version, version, "")
			}
		}
	} else {
		log.Fatal(err)
	}
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func (repo *Repository) appendVersion(version, pathVersion, suffix string) {
	pom := `<?xml version="1.0" encoding="UTF-8"?>
	<project xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd" xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	  <modelVersion>4.0.0</modelVersion>
	  <groupId>%s</groupId>
	  <artifactId>%s</artifactId>
	  <version>%s</version>
	</project>`
	pom = fmt.Sprintf(pom, strings.Replace(repo.Package, "/", ".", 1), "%s", "%s")
	base, basePackageSuffix := repo.getMavenPath(version, suffix)
	repo.Versions[base[:len(base)-len(".jar")]] = makeRepositoryFile(fmt.Sprintf("%s/versions/%s/base.jar", repo.Path, pathVersion), base, fmt.Sprintf(pom, repo.Name, fmt.Sprintf("%s%s", version, suffix)), version)
	repo.Versions[basePackageSuffix[:len(basePackageSuffix)-len(".jar")]] = makeRepositoryFile(fmt.Sprintf("%s/versions/%s/base%s.jar", repo.Path, pathVersion, repo.PackageSuffix), basePackageSuffix, fmt.Sprintf(pom, repo.Name+repo.PackageSuffix, fmt.Sprintf("%s%s", version, suffix)), version)
}

func makeRepositoryFile(filePath, mavenPath, pom, version string) *RepositoryFile {
	hash := getFileHash(filePath)
	return &RepositoryFile{
		FilePath:  filePath,
		MavenPath: mavenPath,
		Checksum:  hash,
		Version:   version,
		Pom:       pom,
		JarFile:   true,
		ServeHTTP: func(w http.ResponseWriter, r *http.Request) {
			if strings.HasSuffix(r.URL.Path, ".sha1") {
				if _, err := w.Write([]byte(hash)); err != nil {
					log.Fatal(err)
				}
			} else if strings.HasSuffix(r.URL.Path, ".pom") {
				w.Header().Set("Content-Type", "application/xml; charset=utf-8")
				if _, err := w.Write([]byte(pom)); err != nil {
					log.Fatal(err)
				}
			} else {
				http.ServeFile(w, r, filePath)
			}
		},
	}
}

func GetRepositoryFile(path string) (*RepositoryFile, error) {
	path = strings.Replace(path, ".jar.sha1", "", 1)
	path = strings.Replace(path, ".jar", "", 1)
	path = strings.Replace(path, ".pom", "", 1)
	path = strings.Replace(path, ".json", "", 1)
	for _, repo := range Repositories {
		if val, ok := repo.Versions[path]; ok {
			return val, nil
		}
	}
	return nil, errors.New("could not find file")
}

func getFileHash(path string) string {
	hasher := sha1.New()
	if f, err := os.Open(path); err == nil {
		if _, err := io.Copy(hasher, f); err == nil {
			if err := f.Close(); err != nil {
				log.Fatal(err)
			}
			return strings.ToTitle(fmt.Sprintf("%x", hasher.Sum(nil)))
		} else {
			log.Fatal(err)
		}
	} else {
		log.Fatal(err)
	}
	return "0"
}

func (repo *Repository) getMavenPath(version, suffix string) (base, basePackageSuffix string) {
	return fmt.Sprintf("%s/%s/%s%s/%s-%s%s.jar", repo.Package, repo.Name, version, suffix, repo.Name, version, suffix),
		fmt.Sprintf("%s/%s%s/%s%s/%s%s-%s%s.jar", repo.Package, repo.Name, repo.PackageSuffix, version, suffix, repo.Name, repo.PackageSuffix, version, suffix)
}

func Export(destination string) {
	for _, repo := range Repositories {
		for path, file := range repo.Versions {
			if file.JarFile {
				if err := os.MkdirAll(fmt.Sprintf("./%s/%s", destination, path[:len(path)-len(strings.Split(path, "/")[len(strings.Split(path, "/"))-1])]), os.ModePerm); err != nil {
					log.Fatal(err)
				}
				if bits, err := ioutil.ReadFile(file.FilePath); err == nil {
					if err := ioutil.WriteFile(fmt.Sprintf("./%s/%s.jar", destination, path), bits, 0777); err != nil {
						log.Fatal(err)
					}
					if err := ioutil.WriteFile(fmt.Sprintf("./%s/%s.jar.sha1", destination, path), []byte(file.Checksum), 0777); err != nil {
						log.Fatal(err)
					}
					if err := ioutil.WriteFile(fmt.Sprintf("./%s/%s.pom", destination, path), []byte(file.Pom), 0777); err != nil {
						log.Fatal(err)
					}
				} else {
					log.Fatal(err)
				}
			}
		}
		log.Printf("Exported repository %s to ./%s/%s/%s/\n", repo.Name, destination, repo.Package, repo.Name)
	}
}

func updateIndex() {
	IndexCache = "Maven server<br><br>"
	for _, repo := range Repositories {
		if repo.AllowIndex {
			for url, data := range repo.Versions {
				if strings.Contains(url, "/") {
					version := strings.Split(url, "/")[len(strings.Split(url, "/")) - 1]
					name := strings.Split(url, "/")[2]
					version = version[len(name) + 1:]
					IndexCache += fmt.Sprintf("<a href=\"%s.jar\">%s:%s:%s</a><br>", url, strings.Replace(repo.Package, "/", ".", 1), name, version)
					IndexCache += fmt.Sprintf("&emsp;%s<br>", data.Checksum)
				}
			}
		}
	}
}

func PopulateRepositories() {
	RepoDataLoaded = false
	Repositories = make(map[string]*Repository)
	if files, err := ioutil.ReadDir("./repositories/"); err == nil {
		for _, file := range files {
			if file.IsDir() {
				if jsonData, err := ioutil.ReadFile(fmt.Sprintf("./repositories/%s/config.json", file.Name())); err == nil {
					config := Repository{}
					if err := json.Unmarshal(jsonData, &config); err == nil {
						config.Path = fmt.Sprintf("./repositories/%s", file.Name())
						config.Name = file.Name()
						config.buildStructure()
						Repositories[file.Name()] = &config
						log.Printf("Loaded repository %s\n", file.Name())
					}
				} else {
					log.Fatal(err)
				}
			}
		}
	} else {
		log.Fatal(err)
	}
	UpdateChangelogs()
	UpdateLatestVersion()
	ReloadVersions()
	updateIndex()
	RepoDataLoaded = true
}
